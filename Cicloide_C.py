#PI CICLOIDE

import numpy as np
import matplotlib.pyplot as plt

L=lambda x,y,x0,y0:np.sqrt(((x-x0)**2)+((y-y0)**2))

r=float(input("Ingrese el radio: "))
d=(2*np.pi)/(float(1000))
Xc=[]
Yc=[]

for k in range(1000):
	Xc.append(r*(k*d-np.sin(k*d)))
	Yc.append(r*(1-np.cos(k*d)))
print("Datos de la cicloide llenos")
print("")

N=input("Ingrese los lados del poligono: ")
Xp=[]
Yp=[]

d=(2*np.pi)/(float(N))
for k in range(N+1):
	Xp.append(r*(k*d-np.sin(k*d)))
	Yp.append(r*(1-np.cos(k*d)))
print("Datos del poligono llenos")
print("")

t=0
for h in range(1,N,1):
	t=L(Xp[h],Yp[h],Xp[h-1],Yp[h-1])+t
print("La longitud es: ")

if r==0.3926880817:
	print(t)
else:
	B=(t*np.pi)/(8*r)
	print(B)
	print("El error es:")
	print(np.pi-B)

#plt.plot(Xc,Yc)
#plt.plot(Xp,Yp)
#plt.show()