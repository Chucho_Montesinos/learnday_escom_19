#PROGRAMA LONGITUD

import numpy as np

L=lambda x,y,x0,y0:np.sqrt(((x-x0)**2)+((y-y0)**2))

n=int(input("Ingrese la cantidad de puntos "))

X=[]
Y=[]

if n==1:

	X=[0]
	Y=[0]
	X.append(int(input("Coordenada X: ")))
	Y.append(int(input("Coordenada Y: ")))

elif n>1:
	
	print("Ingrese las coordenadas X: ")
	for i in range(n):
		X.append(float(input("Coordenada X:" )))
	print("Entrada en X llena")

	print("Ingrese las coordenadas Y")
	for i in range(n):
		Y.append(float(input("Coordenada Y: ")))
	print("Entrada en Y llena")

else:
	print("Error")

t=0
for h in range(1,n,1):
	t=L(X[h],Y[h],X[h-1],Y[h-1])+t
print("La longitud es: ")
print(t)